﻿using System;
using System.Numerics;

namespace ChessTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var board = new Board();

            var position = new Vector2(2, 7);
            var target = new Vector2(1, 7);

            if (board.IsBoardElementAvailable(position) && board.CanMoveBoardElement(position, target))
            {
                Console.WriteLine($"Board element on {position} can move to {target}");

                board.MoveBoardElement(position, target);

                if (!board.IsBoardElementAvailable(position))
                {
                    Console.WriteLine($"Board element succesfully moved to {target}");
                }
            }
        }
    }
}
