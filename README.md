# README #

The objective of the test is to create a simple chessboard system where you are able to place and move around chess units.
https://www.chess.com/terms/chess-pieces

I don't expect castling or pawn's two squares move to be used in this system.

In the base project (Visual studio C# project) you have an example on how to use a Board class but you can edit that project however you need in order to make your chessboard system correctly working.

Once your confident with your system, I invite you to import your source code into a unity project, as an assembly, and to use it in order to provide an UI to it.
Here's an example of what you can get:

Unity build:
https://drive.google.com/file/d/1Mo1wwQAUogDnHLnoAbemBEH7iv2_VJOi/view?usp=sharing

Screens:
![image](https://drive.google.com/uc?export=view&id=1J2zAUD_WgNjl5WbIs1KiJ21UQFohG6Ry)
![image](https://drive.google.com/uc?export=view&id=1DRUt8WbVsQrn-rHqQdVXX9yBdRXxDPnM)